# Green elevator: Design notes

## Instructions
Refer makefile.

## Approach
Initially implemented a naive version which basically spammed the elevators with what they should do, sending the same commands even when the elevator was moving was doing the same.

Realized the communication channel is shared by all the elevators to relay position and control commands. If communication channel is blocked, elevator commands could get delayed and the elevators will start acting weird.

When designing multi threaded/concurrent systems it is good to keep track of the shared resources. in our case it is the communication channel and also standard output/error for logs/error.

Also something special in our case, it is better to ignore a delayed command.

## TODO
Instead of calling nextStop at every round, store result in a variable and update the variable when required with some signal.

## Notes
Needs a mutex for Emergency

### Sync requirements
* condition `notEmergency`
	: `goUp`, `goDown` requires `notEmergency` to be True
* condition `doorClosed`
	: `goUp`, `goDown` requires `doorClosed` to be True
* condition `itenaryNotEmpty`

## Data structure for elevator process
| Type	| Name	| Comment	|
|---|---|:---:|
| `vector<int>`	| `stopList`	| list of floors to be visited	|
| `float`	| `speed`	| speed of the elevator	|
| `bool`	| `speedRefreshed`	| indicates speed has been refreshed	|

## Green logic is in calculateCost
```
calculateCost(floor, direction):
	cost = 0.0
		if going towards floor
			cost += distance to floor + wait delays
			if final direction is opposite
				cost += distance to direction change + wait delays
		else
			cost += distance to direction change + wait delays
			cost += distance to floor + wait delays
```
