# The Green Elevator
**Vishnu Ramesh Maroli <vrmaroli@kth.se>**

## Introduction
The objective of the project was to develop a multi threaded application to understand how to use threads and the design process involved in developing multi threaded applications that run in a distributed environment.

The application developed is an elevator controller. The system consists of two parts:
* The elevator simulator: A java application which is provided as part of the basic code skeleton. It consists of the the elevators and the buttons inside the elevators and on various floors.
* The elevator controller: This was developed as part of the project. It connects to the elevator simulator via TCP sends commands that control the elevators. Button presses, elevator position, speed changes are relayed back by simulator. A base skeleton that implemented the TCP communication and an API to communicate with the simulator was provided.

## Software and computing platforms
* Intel® Core™ i7-3687U CPU @ 2.10GHz × 4
* Ubuntu 17.10
* GNU gcc version 7.2.0
    * pthreads
    * unix sockets

## Design and implementation
Initially implemented a naive version which basically spammed the elevators with what they should do, repeatedly sending the same commands even when the elevator was doing the same. This was an easy implementation but extremely inefficient since the communication channel with the elevator controller is shared by all elevators and was congested. If congestion occurs, control commands could get delayed and the elevators will start acting weird because updates on elevator positions are delayed and response control commands are also delayed further. Example: if a stop command is delayed, the elevator might stop later maybe after it has covered more distance or even worse the elevator could stop mid floor. Also the emergency stop may take forever because of delays.

> **When designing concurrent applications it is important to consider what are the resources that are being shared by the different entities and/or threads.**

A thread safe Elevator class was written which encapsulates the complexities introduced by concurrency.
```
class Elevator {
private:
	static pthread_mutex_t comm_mutex;
	static float speed;

	int id;
	float position;
	enum Moving {Up, No, Down} moving;
	bool doorClosed;
	int destination;
	bool emergency;

	pthread_mutex_t stopList_mutex;
	vector<bool> stopAtFloor;
	int numberOfStops;

	void goUp();
	void goDown();
	void stop();

public:
	Elevator(int _id, int numOfFloors);
	bool isMoving(Moving direction);
	bool isDoorOpen();
	void printStopList();
	void addStop(int floor);
	void triggerEmergency();
	int nextStop();
	void serviceFloor();
	float costToFloor(int floor, FloorButtonType direction);
	void setPosition(float _position);
	void go(Moving direction);
	void run();
};
```
Thread safety is ensured by using two mutexes:
* **comm_mutex**: A static mutex that is shared by all elevators to make sure that only on of the elevators is using the communication channel at a time.
* **stopList_mutex**: Provides atomic read/write guarantee for `stopAtFloor` and `numberOfStops`, as these variables are modified/accessed by multiple threads at the same time.

The state of each elevator is managed by an instance of the `Elevator` class. The `run` method in `Elevator` class runs on a separate thread and sends control commands to the elevators according to its state (position, destination and stop list).

A separate thread manages all information from the elevator simulator and relays them to the appropriate elevator controller.

Since we have encapsulated concurrency, anyone can easily modify the `costToFloor` and `requestElevator` functions to handle the way the elevators prioritise requests. This modification can be done without worrying about the complexities that would otherwise be present because of concurrency.

The current definitions of `costToFloor` and `requestElevator` are far from perfect and improving them is not of interest from the perspective of this project as the concurrency part has already been handled. As it stands now this would only involve figuring out how the elevators should handle requests.

## Performance evaluation
No standardised performance evaluation was done. After the initial naive implementation a new objective added, viz. to avoid congestion in shared resources. The final solution was stress tested to meet this requirement. The elevator controller remained responsive under stress test.

## Conclusion
Implemented an efficient elevator controller which worked perfectly under stress load. During the development, learnt a great deal about designing concurrent systems.
### Key takeaways
* It is good to picture the system as a conversation between threads (Actor model).
* Design should be such that shared resources are used efficiently as this is where bottlenecks are likely to arise.
