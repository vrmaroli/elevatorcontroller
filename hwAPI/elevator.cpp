/* Elevator class initialzation
 * Author: Vishnu Ramesh Maroli
 */

#include "elevator.hpp"

using namespace std;

// static members
float Elevator::speed;
pthread_mutex_t Elevator::comm_mutex = PTHREAD_MUTEX_INITIALIZER;

// Constructor: initial state is assumed.
Elevator::Elevator(int _id, int numOfFloors) {
	id = _id;
	position = 0.0;
	moving = No;
	doorClosed = true;
	destination = 0;
	numberOfStops = 0;
	emergency = false;
	stopList_mutex = PTHREAD_MUTEX_INITIALIZER;

	for(int i = 0; i < numOfFloors; ++i) {
		stopAtFloor.push_back(false);
	}
}

/* Prints the stopList of the elevator.
 * Needs to be protected with sysOut mutex
 */
void Elevator::printStopList() {
	cout << id << ": " ;
	for(int i = 0; i < stopAtFloor.size(); i++) {
		if(stopAtFloor.at(i)) {
			cout << i <<  " " ;
		}
	}
	cout << endl ;
}

bool Elevator::isMoving(Moving direction) {
	return moving == direction;
}

bool Elevator::isDoorOpen() {
	return doorClosed == false;
}

void Elevator::setPosition(float _position) {
	position = _position;
}

void Elevator::go(Moving direction) {
	if(not isMoving(direction)) {
		// check for not emergency here
		pthread_mutex_lock(&comm_mutex);
		switch (direction) {
			case Up:
				goUp();
				break;
			case Down:
				goDown();
				break;
			case No:
				stop();
				break;
			default:
				cerr << "Something went wrong" << endl;
		}
		pthread_mutex_unlock(&comm_mutex);
	}
}

void Elevator::goUp() {
	moving = Up;
	handleMotor(id, MotorUp);
}

void Elevator::goDown() {
	moving = Down;
	handleMotor(id, MotorDown);
}

void Elevator::stop() {
	moving = No;
	handleMotor(id, MotorStop);
}

void Elevator::serviceFloor() {
	pthread_mutex_lock(&comm_mutex);
	stop();
	handleDoor(id, DoorOpen);
	pthread_mutex_unlock(&comm_mutex);

	pthread_mutex_lock(&stopList_mutex);
	stopAtFloor.at(destination) = false;
	numberOfStops--;
	destination = nextStop();
	pthread_mutex_unlock(&stopList_mutex);

	sleep(5);

	pthread_mutex_lock(&comm_mutex);
	handleDoor(id, DoorClose);
	pthread_mutex_unlock(&comm_mutex);
}

float Elevator::costToFloor(int _floor, FloorButtonType direction) {
	/* I guess a better cost function should consider the direction the user
	 * wants to go too. However since this is not topic of interest here, using
	 * a naive cost funciton
	 */
	int movingCost = 1, waitingCost = 3;
	int distance = 0;
	int stopsOnTheWay = 0;
	Moving wannaGo = (direction == GoingUp) ? Up : Down;
	if(isMoving(Up)) {
		if(position < _floor) {
			distance = _floor - position;
			for(int i = ceil(position); i < _floor; i++) if(stopAtFloor.at(i))
				stopsOnTheWay++;
		}
		else {
			distance = abs(_floor - position)
				+ stopAtFloor.size()
				- min(_floor, (int)ceil(position));
			stopsOnTheWay = numberOfStops;
		}
	}
	else if(isMoving(Down)) {
		if(_floor < position) {
			distance = position - _floor;
			for(int i = ceil(_floor); i < position; i++) if(stopAtFloor.at(i))
				stopsOnTheWay++;
		}
		else {
			distance = abs(_floor - position)
				+ stopAtFloor.size()
				- max(_floor, (int)ceil(position));
			stopsOnTheWay = numberOfStops;
		}
	}
	else {
		// Bug: wrong cost if the lift is sericing a floor because moving==No
		distance = abs(_floor - position);
		stopsOnTheWay = numberOfStops;
	}
	return (distance * movingCost)
		+ (waitingCost * stopsOnTheWay);
}

void Elevator::addStop(int _floor) {
	pthread_mutex_lock(&stopList_mutex);
	if(not stopAtFloor.at(_floor)) {
		stopAtFloor.at(_floor) = true;
		numberOfStops++;
	}
	pthread_mutex_unlock(&stopList_mutex);
	destination = nextStop();
}

void Elevator::triggerEmergency() {
	emergency = true;
}

int Elevator::nextStop() {
	if(isMoving(Up)) {
		int ret = ceil(position);
		while(ret < stopAtFloor.size() and not stopAtFloor.at(ret)) ret++;
		if(ret < stopAtFloor.size()) {
			return ret;
		}
		ret = ceil(position);
		while(ret >= 0 and not stopAtFloor.at(ret)) ret--;
		if(ret >= 0) {
			return ret;
		}
	}
	else if(isMoving(Down)) {
		int ret = floor(position);
		while(ret >= 0 and ret < stopAtFloor.size() and not stopAtFloor.at(ret)) ret--;
		if(ret >= 0) {
			return ret;
		}
		ret = floor(position);
		while(ret < stopAtFloor.size() and not stopAtFloor.at(ret)) ret++;
		if(ret < stopAtFloor.size()) {
			return ret;
		}
	}
	else {
		// chooses nearest floor which is waiting. Maybe this is not the best way to choose
		int retUp = ceil(position);
		int retDown = floor(position);
		while(retUp < stopAtFloor.size() and retUp >= 0 and not stopAtFloor.at(retUp)) retUp++;
		while(retDown >= 0 and retDown < stopAtFloor.size() and not stopAtFloor.at(retDown)) retDown--;
		if(retUp < stopAtFloor.size() and retDown >= 0) {
			float distUp = abs(position - retUp);
			float distDown = abs(position - retDown);
			if(distUp < distDown) {
				return retUp;
			}
			else {
				return retDown;
			}
		}
		else if(retUp < stopAtFloor.size()) {
			return retUp;
		}
		else if(retDown >= 0) {
			return retDown;
		}

		return -1;
	}
}

void Elevator::run() {
	while (true) {
		if(emergency) {
			pthread_mutex_lock(&comm_mutex);
			stop();
			pthread_mutex_unlock(&comm_mutex);
			sleep(5);
			emergency = false;
			// problem is busy waiting, kinda
		}
		else if(numberOfStops > 0) {
			if(abs(position - destination) <= 0.05) {
				serviceFloor();
			}
			else if(abs(position - destination) > 0.05 and position < destination) {
				go(Up);
			}
			else {
				go(Down);
			}
		}
		else
			sleep(1);
	}
}
