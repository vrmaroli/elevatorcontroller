/* Elevator class
 * Author: Vishnu Ramesh Maroli
 */

#ifndef __ELEVATOR_H
#define __ELEVATOR_H

#include <iostream>
#include <cmath>
#include <vector>

#include <unistd.h>
#include <pthread.h>

#include "hardwareAPI.h"

using namespace std;

class Elevator {
private:
	static pthread_mutex_t comm_mutex;
	static float speed;

	int id;
	float position;
	enum Moving {Up, No, Down} moving;
	bool doorClosed;
	int destination;
	bool emergency;

	pthread_mutex_t stopList_mutex;
	vector<bool> stopAtFloor;
	int numberOfStops;

	void goUp();
	void goDown();
	void stop();

public:
	Elevator(int _id, int numOfFloors);
	bool isMoving(Moving direction);
	bool isDoorOpen();
	void printStopList();
	void addStop(int floor);
	void triggerEmergency();
	int nextStop();
	void serviceFloor();
	float costToFloor(int floor, FloorButtonType direction);
	void setPosition(float _position);
	void go(Moving direction);
	void run();

	static void setSpeed(float _speed) {
		speed = _speed;
	}
};

#endif
