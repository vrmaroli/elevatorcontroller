/* Elevator Controller
 * Author: Vishnu Ramesh Maroli
 */

#include <iostream>
#include <vector>
#include <limits>

#include <pthread.h>

#include "hardwareAPI.h"
#include "elevator.hpp"

using namespace std;

void *commProcess(void *unused);
void requestElevator(int _floor, FloorButtonType direction);

/* sysOut_mutex
 * makes sure stdout is used only by one thread at a time
 */
pthread_mutex_t sysOut_mutex = PTHREAD_MUTEX_INITIALIZER;

vector<Elevator> elevators;

void requestElevator(int _floor, FloorButtonType direction) {
	vector<Elevator>::iterator i = elevators.begin();
	vector<Elevator>::iterator best = i;
	int bestCost = numeric_limits<int>::max();
	while(i < elevators.end()) {
		int cost = i->costToFloor(_floor, direction);
		if(cost < bestCost) {
			bestCost = cost;
			best = i;
		}
		i++;
	}
	best->addStop(_floor);
}

void *commProcess(void *unused) {
	EventType e;
	EventDesc ed;
	while (1) {
		e = waitForEvent(&ed);
		switch (e) {
			case FloorButton:
				requestElevator(ed.fbp.floor, ed.fbp.type);
				break;
			case CabinButton:
				if(ed.cbp.floor == 32000) {
					elevators.at(ed.cbp.cabin - 1).triggerEmergency();
				}
				else {
					elevators.at(ed.cbp.cabin - 1).addStop(ed.cbp.floor);
				}

				pthread_mutex_lock(&sysOut_mutex);
				elevators.at(ed.cbp.cabin -1).printStopList();
				pthread_mutex_unlock(&sysOut_mutex);
				break;
			case Position:
				elevators.at(ed.cbp.cabin - 1).setPosition(ed.cp.position);
				break;
			case Speed:
				Elevator::setSpeed(ed.s.speed);
				break;
			case Error:
				break;
		}
	}
}

void *helperFunction(void* arg) {
	int* i = (int*)arg;
	elevators.at(*i).run();
}

int main(int argc, char **argv) {
	char *hostname;
	int port;
	int numOfElevators, numOfFloors;

	if (argc != 5) {
		fprintf(stderr, "Usage: %s host-name port numOfElevators numOfFloors\n", argv[0]);
		fflush(stderr);
		exit(-1);
	}
	hostname = argv[1];
	if ((port = atoi(argv[2])) <= 0) {
		fprintf(stderr, "Bad port number: %s\n", argv[2]);
		fflush(stderr);
		exit(-1);
	}
	if ((numOfElevators = atoi(argv[3])) <= 0) {
		fprintf(stderr, "Bad numOfElevators: %s\n", argv[2]);
		fflush(stderr);
		exit(-1);
	}
	if ((numOfFloors = atoi(argv[4])) <= 0) {
		fprintf(stderr, "Bad numOfFloors: %s\n", argv[2]);
		fflush(stderr);
		exit(-1);
	}

	initHW(hostname, port);

	int id[numOfElevators];
	for(int i = 0; i < numOfElevators; i++) {
		elevators.push_back(Elevator(i + 1, numOfFloors + 1));
		id[i] = i;
	}

	pthread_t newThread[numOfElevators];
	for (int i = 0;i < elevators.size(); i++) {
		pthread_create(&newThread[i], NULL, &helperFunction, (void*)(id + i));
		pthread_detach(newThread[i]);
	}

	pthread_t commThread;
	pthread_create(&commThread, NULL, commProcess, (void *) NULL);
	pthread_join(commThread, NULL);

	return 0;
}
